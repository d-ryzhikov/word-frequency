LOCALS @@
.model small
.stack 100h

DictItem struc
    address dw ?
    frequency db ?
DictItem ends

.data
buf db 255, 257 dup (0)
processed_buf db 256 dup (0)
divider db 10
num_str db 3 dup(?)

word_start dw ?
words_count db 0
dict DictItem 128 dup (<>)

new_line db 0Ah,0Dh,'$'
dash db " -> ",'$'
.code
jmp main

get_string proc
    push ax
    push dx

    mov ah,0Ah
    mov dx,offset buf
    int 21h

    pop dx
    pop ax
    ret
endp get_string

clean_string proc
;ds:si=string to process
;ds:di=output string
@@next_char:
    lodsb
@@check_char:
    test al,al
    jz @@string_end
    cmp al,' '
    je @@white_space
    cmp al,9    ;tab
    je @@white_space
    cmp al,'A'
    jb @@next_char
    cmp al,'z'
    ja @@next_char
    cmp al,'Z'
    jbe @@save
    cmp al,'a'
    jb @@next_char
@@save:
    stosb
    jmp @@next_char
@@white_space:
    stosb
    @@skip_spaces:
        lodsb
        cmp al,' '
        je @@skip_spaces
        cmp al,9
        je @@skip_spaces
        jmp @@check_char
@@string_end:
    ret
endp clean_string

get_word_stats proc
;ds:si=string address
@@outside_word:
    lodsb
    test al,al
    jz @@to_string_end
    cmp al,' '
    je @@outside_word
    cmp al,9
    je @@outside_word
    jmp @@in_word
    @@to_string_end:
        jmp @@string_end
@@in_word:
    dec si
    mov [word_start],si
    xor cx,cx
    mov cl,[words_count]
    dec cx
    lea di,dict
    mov ax,cx
    mov dx,type DictItem
    mul dx
    add di,ax
    @@lookup:
        cmp cx,0
        jl @@add_word
        mov si,[word_start]
        mov bx,[di].address
        @@cmp_chars:
            lodsb
            test al,al
            jz @@word_end
            cmp al,' '
            je @@word_end
            cmp al,9
            je @@word_end
            cmp al,byte ptr [bx]
            jne @@not_eq
            inc bx
            jmp @@cmp_chars
        @@word_end:
            mov al,byte ptr [bx]
            cmp al,' '
            je @@eq
            cmp al,9
            je @@eq
        @@not_eq:
            mov dx,type DictItem
            sub di,dx
            dec cx
            jmp @@lookup
        @@eq:
            inc [di].frequency
            jmp @@outside_word
    @@skip_word:
        lodsb
        test al,al
        jz @@string_end
        cmp al,' '
        je @@outside_word
        cmp al,9
        je @@outside_word
        jmp @@skip_word
    @@add_word:
        xor ax,ax
        mov al,[words_count]
        mov dx,type DictItem
        mul dx
        lea di,[dict]
        add di,ax
        mov bx,[word_start]
        mov [di].address,bx
        mov [di].frequency,1
        inc [words_count]
        jmp @@skip_word

@@string_end:
    ret
endp get_word_stats

print_stats proc
;ds:di=dict address
    xor cx,cx
    mov bx,type DictItem
@@main_cycle:
    cmp cl,[words_count]
    jnb @@exit

    mov ah,09h
    lea dx,new_line
    int 21h

    mov si,[di].address
    mov ah,02h
    @@print_cycle:
        lodsb
        test al,al
        jz @@word_end
        cmp al,' '
        je @@word_end
        cmp al,9
        je @@word_end
        mov dl,al
        int 21h
        jmp @@print_cycle
    @@word_end:
        mov ah,09h
        lea dx,dash
        int 21h

        mov dl,[di].frequency
        call print_int

        add di,bx
        inc cx
        jmp @@main_cycle
@@exit:
    ret
endp print_stats

print_int proc
;dl=int to print
    push ax
    push cx
    push di

    lea di,num_str
    mov al,dl
    xor cx,cx
@@next_num:
    xor ah,ah
    div [divider]
    add ah,'0'
    xchg al,ah
    stosb
    xchg al,ah
    inc cx
    test al,al
    jnz @@next_num

    mov ah,02h
@@print:
    dec di
    mov dl,[di]
    int 21h
    dec cx
    jnz @@end

@@end:
    pop di
    pop cx
    pop ax
    ret
endp print_int

main:
    mov ax,@data
    mov ds,ax
    mov es,ax

    cld

    call get_string
    lea si,buf+2
    lea di,processed_buf
    call clean_string

    lea si,processed_buf
    call get_word_stats

    lea di,dict
    call print_stats

    mov ax,4c00h
    int 21h
end main